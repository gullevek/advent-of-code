#!/usr/bin/perl

use strict;

# op[1]: add from two, store in third
#        first two show pos from where to read, third where to save
# op[2]: multiply
# op done -> move 4 pos
# op[99]: end
# op[other]: error

# before start: pos 1 with value 12
#               pos 2 with value 2

my $base_data = 'base_data.txt';
my @opcodes = ();
# shift for each action
my $shift = 4;
# read in data
open(FP, '<', $base_data) || die('Cannot open file');
while (<FP>) {
	chomp $_;
	@opcodes = split(/,/, $_);
}
close(FP);
# INIT
$opcodes[1] = 12;
$opcodes[2] = 2;
# loop and shift by pos for
for (my $pos = 0; $pos < @opcodes; $pos += $shift) {
	print "OP[$pos]: ".$opcodes[$pos].": ";
	# pos 1,2 for values, 3 for target
	my $pos_1 = $opcodes[$pos + 1];
	my $pos_2 = $opcodes[$pos + 2];
	my $target = $opcodes[$pos + 3];
	# read in values
	my $value_1 = $opcodes[$pos_1];
	my $value_2 = $opcodes[$pos_2];
	# code run
	if ($opcodes[$pos] == 1) {
		# add
		$opcodes[$target] = $value_1 + $value_2;
		print "{".$pos_1."}".$value_1." + {".$pos_2."}".$value_2." = {".$target."}".$opcodes[$target]."\n";
	} elsif ($opcodes[$pos] == 2) {
		# multiply
		$opcodes[$target] = $value_1 * $value_2;
		print "{".$pos_1."}".$value_1." * {".$pos_2."}".$value_2." = {".$target."}".$opcodes[$target]."\n";
	} elsif ($opcodes[$pos] == 99) {
		print "End Program\n";
		last;
	} else {
		die("Abort Error: ".$opcodes[$pos]);
	}
}
print "Pos[0]: ".$opcodes[0]."\n";

__END__
