#!/usr/bin/perl

use strict;
use POSIX;

my $base_data = 'base_data.txt';
my $fuel_needed = 0;
my $fuel_needed_full = 0;

# rule:
# PART 1
# 1) devied by 3
# 2) floor
# 3) -2
# PART 2
# weight now recursive calc above until we have 0 or negative

open(FP, '<', $base_data) || die('Cannot open file');
while (<FP>) {
	chomp $_;
	# we should check if this is a number, but meh
	my $_fuel = $_;
	# reset Part 1 counter
	my $first = 0;
	# continue as long we have fuel left to calculate
	while ($_fuel > 0) {
		$_fuel = floor($_fuel / 3) - 2;
		# only on first loop, for Part 1
		if (!$first) {
			$fuel_needed += $_fuel;
			$first = 1;
		}
		# continue for Part 2 as long as we are above 0
		if ($_fuel > 0) {
			$fuel_needed_full += $_fuel;
		}
	}
}

print "Fuel Part 1: ".$fuel_needed."\n";
print "Fuel Part 2: ".$fuel_needed_full."\n";

close(FP);
